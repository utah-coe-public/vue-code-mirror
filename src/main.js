import VueCodeMirror from './components/VueCodeMirror.vue';

import { library } from '@fortawesome/fontawesome-svg-core'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons'
import { faSpinner } from '@fortawesome/free-solid-svg-icons'
import { faExpand } from '@fortawesome/free-solid-svg-icons'
import { faExpandArrowsAlt } from '@fortawesome/free-solid-svg-icons'
import { faArrowsAltH } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

import 'codemirror/theme/lesser-dark.css';
import 'codemirror/theme/elegant.css';
import 'codemirror/lib/codemirror.css';
import 'codemirror/addon/fold/foldgutter.css';
import 'codemirror/addon/dialog/dialog.css';
import 'codemirror/theme/monokai.css';

library.add(faTimes)
library.add(faCheckCircle)
library.add(faSpinner)
library.add(faExpand)
library.add(faExpandArrowsAlt)
library.add(faArrowsAltH)


Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false

export default VueCodeMirror;
