import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import serve from 'rollup-plugin-serve';
import pkg from './package.json';
import livereload from 'rollup-plugin-livereload';
import vue from 'rollup-plugin-vue'
import postcss from 'rollup-plugin-postcss';

 const globals = {

};

export default [
	// browser-friendly UMD build
	{
		input: 'src/main.js',
        external: [],
		output: {
			name: 'VueCodeMirror',
			file: pkg.browser,
			format: 'umd',
            globals
		},
		plugins: [
            vue(),
            postcss({
                extensions: [ '.css' ],
            }),
			resolve(), // so Rollup can find `ms`
			commonjs(), // so Rollup can convert `ms` to an ES module
            serve({
                open: true,
                contentBase: 'example',

            }),
            livereload({
                watch: ['dist', 'example']
            })
		]
	},

];
